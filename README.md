<h1 align="center">MV-Documentation</h1> <br>
<!--<p align="center">
    <img alt="mv-documentation logo" title="mv-documentation logo" src="#" width="300">
</p>-->
<div align="center">
  <strong>Documentation catalog</strong>
</div>
<div align="center">
  Browse Documentation files in the Valais Media Library for the members and library staff.
</div>

<div align="center">
  <h3>
    <a href="https://valais-media-library.gitlab.io/mv-documentation/">Access</a>
    <span> | </span>
    <a href="#documentation">Documentation</a>
    <span> | </span>
    <a href="#contributing">
      Contributing
    </a>
  </h3>
</div>

<div align="center">
  <sub>Built with ❤︎ in Valais by <a href="https://gitlab.com/valais-media-library/mv-documentation/-/graphs/master">contributors</a>
</div>


## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Usage](#usage)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [History and changelog](#history-and-changelog)
- [Authors and acknowledgment](#authors-and-acknowledgment)

## Introduction

[![license](https://img.shields.io/badge/license-MIT-green.svg?style=flat-square)](https://gitlab.com/valais-media-library/mv-documentation/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/valais-media-library/mv-documentation/badges/master/pipeline.svg)](https://gitlab.com/valais-media-library/mv-documentation/commits/master)


👉 [View online](https://valais-media-library.gitlab.io/mv-documentation/)

## Features

- Browse and search documentation

## Documentation

All documentation files are in the `/documentation` directory. Each time a file is created, it must be listed in the `/documentation/_list.json` file. The name of the created file must be in lower case separated by dashes, contain the name of the product and the language in which it is written. For example: `canon-lide-400-fr`.

The structure of the json file must be similar in every aspect to that of the `/documentation/example.json` file and be valid. Validation can be done [here](https://jsonformatter.org/).

### Installation

[Download the zip file](https://gitlab.com/valais-media-library/mv-documentation/-/archive/master/mv-documentation-master.zip) from the latest release and unzip it somewhere reachable by your webserver.

### GitLab CI

This project's static Pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/) every time you push or update the repository, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml). It expects to put all your HTML files in the `public/` directory.

## Contributing

We’re really happy to accept contributions from the community, that’s the main reason why we open-sourced it! There are many ways to contribute, even if you’re not a technical person. Check the [roadmap](#roadmap) or [create an issue](https://gitlab.com/valais-media-library/mv-documentation/issues) if you have a question.

1. [Fork](https://help.github.com/articles/fork-a-repo/) this [project](https://gitlab.com/valais-media-library/mv-documentation/)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## History and changelog

You will find changelogs and releases in the [CHANGELOG](https://gitlab.com/valais-media-library/mv-documentation/blob/master/CHANGELOG) file.

## Roadmap

Nothing

## Authors and acknowledgment

* **Michael Ravedoni** - *Initial work* - [michaelravedoni](https://gitlab.com/michaelravedoni)

See also the list of [contributors](https://github.com/michaelravedoni/prathletics/contributors) who participated in this project.

* [Vue.js](https://github.com/vuejs/vue) Vue.js is a progressive, incrementally-adoptable JavaScript framework for building UI on the web.

## License

[MIT License](https://gitlab.com/valais-media-library/mv-documentation/blob/master/LICENSE)
